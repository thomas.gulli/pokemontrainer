import {Component} from '@angular/core';
import {User} from '../../../shared/models/user.model';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html'
})
export class LoginPage {

  constructor(private readonly router: Router) {
  }

  // When handleLogin, it navigates to the trainer page, and saves the user data to localstorage
  handleLogin(user: User): Promise<boolean>  {
    localStorage.setItem('user', JSON.stringify(user));
    return this.router.navigate([AppRoutes.Trainer]);
  }
}
