import {Injectable} from '@angular/core';
import {LoginState} from './state/login.state';
import {Observable} from 'rxjs';
import {User} from '../shared/models/user.model';


@Injectable({
  providedIn: 'root'
})

export class LoginFacade {
  constructor(private readonly loginState: LoginState) {
  }

  public user$(): Observable<User>{
    return this.loginState.getUser$();
  }

  // When login, the facade sets the user, with the given username
  public login(username: string): void {
    if (username !== ''){
      this.loginState.setUser({username, collectedPokemons: []});
    }
  }


}
