
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {Pokemon} from '../../shared/models/pokemon.model';
import {map, shareReplay} from 'rxjs/operators';
import {PokemonResponse} from '../../shared/models/pokemon-response.model';

const { pokemonApiBaseUrl } = environment;
@Injectable({
  providedIn: 'root'
})
export class PokemonApi {

  private readonly pokemonCache$; // Cached data from the api
  public pokemons: Pokemon[] = [];
  public error: string = '';
  public count: number;
  private offset = 0;
  private limit = 20;
  public counts = 0;
  public isLastPage: boolean = false;
  public isFirstPage: boolean = true;

  // Returns data from the api with http.get
  constructor(private readonly http: HttpClient){
    this.pokemonCache$ = this.http.get(`${ pokemonApiBaseUrl }pokemon?limit=1000`).pipe(shareReplay(1));
  }
  public getPokemon(): Pokemon[]{
    return this.pokemons.slice(
      this.offset,
      this.offset + this.limit
    );
  }

  // Fetches the pokemon from the api to the pokemons list, and finds id and image, which is not stored directly in the api
  public fetchPokemon(): void{
      this.pokemonCache$.pipe(
        map((response: PokemonResponse) => {
          return response.results.map(pokemon => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url)
        }));
      }))
      .subscribe(
        (pokemon: Pokemon[]) => {
          this.pokemons = pokemon;
          this.counts = this.pokemons.length;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

  // Returns the id and image, based on the url
  private getIdAndImage(url: string): {id: string, image: string} {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }

  // Gets the details for a pokemon with a given url
  public pokemonDetails(url: string): Observable<Pokemon>{
    return this.http.get<Pokemon>(url);
  }

  // Updates the offset for fetching next pokemons
  public next(): void{
    const nextOffset = this.offset + this.limit;
    if (nextOffset <= this.counts - 1){
    this.offset += this.limit;
    }
    this.page();
  }

  // Updates the offset for fetching previous pokemons
  public prev(): void{
    if (this.offset !== 0){
      this.offset -= this.limit;
    }
    this.page();

  }

  public page(): void{
    this.isLastPage = this.offset === this.counts - this.limit;
    this.isFirstPage = this.offset === 0;
  }
}
