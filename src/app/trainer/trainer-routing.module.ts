import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TrainerPage} from './pages/trainer.page';
import {PokemonContainerModule} from '../components/pokemon-container/pokemon-container.module';
const routes: Routes = [
  {
    path: '',
    component: TrainerPage,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( routes ),
    PokemonContainerModule
  ],
  exports: [
    RouterModule,
    PokemonContainerModule
  ]
})
export class TrainerRoutingModule {}
