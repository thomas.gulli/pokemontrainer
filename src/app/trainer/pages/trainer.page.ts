import { Component } from '@angular/core';
import {User} from '../../shared/models/user.model';
import {PokemonApi} from '../../pokemon/api/pokemon.api';
import {Pokemon} from '../../shared/models/pokemon.model';

@Component({
  selector: 'app-catalogue',
  templateUrl: 'trainer.page.html',
})

export class TrainerPage {
  public response: Pokemon;
  constructor(private readonly pokemonAPI: PokemonApi) { }

  public user: User = JSON.parse(localStorage.getItem('user')); // Gets the user from localstorage
}
