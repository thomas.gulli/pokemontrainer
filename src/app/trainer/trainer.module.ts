import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrainerPage} from './pages/trainer.page';
import {TrainerRoutingModule} from './trainer-routing.module';

@NgModule({
  declarations: [
    TrainerPage
  ],
  imports: [
    CommonModule,
    TrainerRoutingModule
  ]
})
export class TrainerModule {}
