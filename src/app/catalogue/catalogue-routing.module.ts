import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CataloguePage} from './pages/catalogue.page';
import { PokemonContainerModule } from '../components/pokemon-container/pokemon-container.module';

const routes: Routes = [
  {
    path: '',
    component: CataloguePage,
  }
];

@NgModule({
  imports: [
    PokemonContainerModule,
    RouterModule.forChild( routes ),
  ],
  exports: [
    RouterModule,
    PokemonContainerModule,
  ]
})
export class CatalogueRoutingModule {}
