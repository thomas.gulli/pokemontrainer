import {Component, OnInit} from '@angular/core';
import {PokemonApi} from 'src/app/pokemon/api/pokemon.api';
import {Pokemon} from '../../shared/models/pokemon.model';

@Component({
  selector: 'app-catalogue',
  templateUrl: 'catalogue.page.html',
  styleUrls: ['catalogue.page.css']
})

export class CataloguePage implements OnInit {

  constructor(private readonly pokemonAPI: PokemonApi) {
  }

  // Returns the pokemons list
    get pokemon(): any[]{
      return this.pokemonAPI.getPokemon();
    }

  // Fetches the pokemons at the start/ on init.
  ngOnInit(): void {
    this.pokemonAPI.fetchPokemon();
  }

  // Sets a boolean to see if the view is the first page
  get isFirstPage(): boolean {
    return this.pokemonAPI.isFirstPage;
  }

  // Sets a boolean to see if the view is the last page
  get isLastPage(): boolean {
    return this.pokemonAPI.isLastPage;
  }

  // Fetches next pokemons
  onNextClick(): void {
    this.pokemonAPI.next();
  }

  // Fetches the previous pokemons
  onPrevClick(): void {
    this.pokemonAPI.prev();
  }
}
