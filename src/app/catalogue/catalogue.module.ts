import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CatalogueRoutingModule} from './catalogue-routing.module';
import {CataloguePage} from './pages/catalogue.page';

@NgModule({
  declarations: [
    CataloguePage,
  ],
  exports: [
  ],
  imports: [
    CommonModule,
    CatalogueRoutingModule
  ]
})
export class CatalogueModule {}
