import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { SessionLoginGuard } from './guards/session-login.guard';
import { SessionGuard } from './guards/session.guard';
import {AppRoutes} from './shared/enums/app-routes.enum';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: AppRoutes.Login,
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    canActivate: [SessionLoginGuard]

  },
  {
    path: AppRoutes.Trainer,
    loadChildren: () => import('./trainer/trainer.module').then(m => m.TrainerModule),
    canActivate: [SessionGuard]
  },
  {
    path: AppRoutes.Catalogue,
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule),
    canActivate: [SessionGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
