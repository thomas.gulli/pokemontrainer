import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {User} from '../../shared/models/user.model';
import {Subscription} from 'rxjs';
import {LoginFacade} from '../../login/login.facade';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent implements OnDestroy{

  // when loggedIn.emit is called, the user data is sent from the component.
  @Output() loggedIn: EventEmitter<User> = new EventEmitter<User>();

  public username: string = '';
  private user$: Subscription;

  // Saves the user in the user state in loginFacade when logged in.
  constructor(private readonly loginFacade: LoginFacade) {
    this.user$ = this.loginFacade.user$().subscribe((user: User) => {
      if (user === null){
        return;
      }
      this.loggedIn.emit(user);
    });
  }

  // Call from the login button at the html
  public onLoginClick(): void{
    this.loginFacade.login(this.username);
  }

  ngOnDestroy(): void {
    this.user$.unsubscribe();
  }
}
