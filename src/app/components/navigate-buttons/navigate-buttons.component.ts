import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppRoutes} from '../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-navigate-buttons',
  templateUrl: 'navigate-buttons.component.html'
})

// Shared buttons for all pages for navigating
export class NavigateButtonsComponent{
  constructor(private readonly router: Router) {
  }

  toCatalogue(): Promise<boolean> {
    return this.router.navigate([AppRoutes.Catalogue]);
  }

  toTrainer(): Promise<boolean> {
    return this.router.navigate([AppRoutes.Trainer]);
  }

  logout(): Promise<boolean> {
    localStorage.clear();
    return this.router.navigate([AppRoutes.Login]);
  }
}
