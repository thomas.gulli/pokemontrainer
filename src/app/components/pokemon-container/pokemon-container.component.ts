import {Component, Input} from '@angular/core';
import {Pokemon} from '../../shared/models/pokemon.model';
import {PokemonApi} from 'src/app/pokemon/api/pokemon.api';
import {User} from '../../shared/models/user.model';

@Component({
  selector: 'app-pokemon-container',
  templateUrl: './pokemon-container.component.html',
  styleUrls: ['pokemon-container.component.css']
})
export class PokemonContainerComponent {
  constructor(private readonly pokemonAPI: PokemonApi) {
  }

  // Gets a pokemonList from parent component/ page
  @Input() pokemonList: Pokemon[];

  public pokemon: Pokemon;
  public user: User = JSON.parse(localStorage.getItem('user')); // User stored locally with localStorage

  // Gets details for a given pokemons, for showing details
  showPokemonCard(pokemon: Pokemon): void {
    this.pokemonAPI.pokemonDetails(pokemon.url).subscribe((response: Pokemon) => {
      this.pokemon = response;
      this.pokemon.image = pokemon.image; // Overwrite with the image from the "old" object, when no image is declared in the pokemons details
    });
  }

  // Saves a pokemons to the user, when a pokemons is collected. Here we are using localstorage for that.
  savePokemon(pokemon: Pokemon): void {
    if (this.user.collectedPokemons.filter(p => p.id === pokemon.id).length === 0) { // Stores if the user has not the pokemons before
      this.user.collectedPokemons.push(pokemon);
      localStorage.setItem('user', JSON.stringify(this.user));
    }
  }
}
