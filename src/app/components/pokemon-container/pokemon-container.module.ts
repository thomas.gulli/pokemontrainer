import { NgModule } from '@angular/core';
import { PokemonContainerComponent } from './pokemon-container.component';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [CommonModule],
    exports: [PokemonContainerComponent],
    declarations: [PokemonContainerComponent],
    providers: [],
 })

 export class PokemonContainerModule {
 }
