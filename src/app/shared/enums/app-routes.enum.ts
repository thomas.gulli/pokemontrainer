export enum AppRoutes{
  Login = 'login',
  Trainer = 'trainer',
  Catalogue = 'catalogue'
}
