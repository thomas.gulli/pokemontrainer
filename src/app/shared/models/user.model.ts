import { Pokemon } from "./pokemon.model";

export interface User {
  username: string;
  collectedPokemons: Array<Pokemon>;
}
