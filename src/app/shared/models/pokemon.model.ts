export interface Pokemon{
  id?: string;
  name: string;
  url: string;
  image?: string;
  height?: string;
  weight?: string;
  base_experience?: string;
  abilities?: PokemonAbilities[];
  stats?: PokemonStats[];
  moves?: PokemonMoves[];
  sprites?: PokemonSprites[];
  types?: PokemonType[];
}

export interface PokemonType{
  slot: number;
  type: PokemonTypeType[];
}
export interface PokemonTypeType{
  name: string;
  url: string;
}
export interface PokemonStats{
  base_stat: number;
  effort: number;
  stat: PokemonStatsStat[];
}
export interface PokemonStatsStat{
  name: string;
  url: string;
}
export interface PokemonSprites{
  front_shiny: string;
  back_shiny: string;
  other: PokemonSpritesOther;
}
export interface PokemonSpritesOther{
  dream_world: string;
  'official-artwork': PokemonSpritesOfficial;
}
export interface PokemonSpritesOfficial{
  front_default: string;
  'official-artwork': any;
}


export interface  PokemonAbilities{
  ability: Ability;
}
export interface Ability{
  name: string;
}
export interface PokemonMoves{
  move: Move;
}
export interface Move{
  name: string;
}
