import { Injectable } from '@angular/core';
import { AppRoutes } from '../enums/app-routes.enum';


@Injectable({
    providedIn: 'root'
})
export class SessionService{

    // returns a boolean based on the user is set or not
    active(): boolean{
        const trainer = localStorage.getItem('user');
        return Boolean(trainer);
    }
}
