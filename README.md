# Pokemon catalogue

Pokemon catalogue web app using the Angular Framework. It displays pokemons from the pokeapi.com api, and makes a user to collect some pokemons to the trainer page.
When the web application starts, the user must log in before collecting pokemons, and before the user can go to his trainer page.
When loged in, the application redirects to the trainer page. There the user can see the collected pokemons. It is also buttons for navigating to for example the catalogue page, where the user can collects pokemons to the trainer page. 
When clicking on a pokemon card, the details for the pokemon is showed at the right side. 

## Participants
Hiba Benhaida and Thomas Gulli

## Technologies
- Typescript
- Angular
- HTML
- CSS

# Credits

Thanks to our teacher Dewald at Noroff at the fullstack Java course for guidance. Some of this code is heavily inspired from his projects. 
